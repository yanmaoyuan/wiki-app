package ir.wiki;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Configuration {
	// dictionary file
	public static final File DICTIONARY_FILE = new File("data/dictionary.bin");
	// directory for all of the documents
	public static final Path DOCUMENT_DIR = Paths.get("data/docs/IR");

	// the root for all differe types of models
	public static final Path MODEL_BASE_DIR = Paths.get("data/models");
	// cached wiki model file
	public static final String WIKI_MODEL_FILE = "wiki_model.bin";
	// cached document model file
	public static final String DOCUMENT_MODEL_FILE = "document_model_caleb.bin";

	// query file
	public static final File QUERY_FILE = new File("data/query/query.txt");

	private final ModelType modelType;
	private final LinkWeight linkWeight;
	private final File wikiModelFile;
	private final File documentModeFile;

	private Configuration(Builder builder) {
		this.modelType = builder.modelType;
		this.linkWeight = builder.linkWeight;

		final Path modelBaseDir = MODEL_BASE_DIR.resolve(modelType.path)
				.resolve(linkWeight.path);
		this.wikiModelFile = modelBaseDir.resolve(WIKI_MODEL_FILE).toFile();
		this.documentModeFile = modelBaseDir.resolve(DOCUMENT_MODEL_FILE)
				.toFile();
	}

	public File getWikiModelFile() {
		return wikiModelFile;
	}

	public File getDocumentModelFile() {
		return documentModeFile;
	}

	public ModelType modelType() {
		return modelType;
	}

	public LinkWeight linkWeightType() {
		return linkWeight;
	}

	public static class Builder {
		/*
		 * Default to build unsmoothed and zero link weights
		 */
		private ModelType modelType = ModelType.UNSMOOTH;
		private LinkWeight linkWeight = LinkWeight.ZERO_WEIGHT;

		public Builder modelType(ModelType modelType) {
			this.modelType = modelType;
			return this;
		}

		public Builder linkWeight(LinkWeight linkWeight) {
			this.linkWeight = linkWeight;
			return this;
		}

		public Configuration build() {
			return new Configuration(this);
		}
	}

	public enum ModelType {
		BINARY(null), LINDSTONE("lindstone"), WITTENBELL("wittenbell"), UNSMOOTH(
				"unsmooth");

		private final String path;

		ModelType(String path) {
			this.path = path;
		}
	}

	public enum LinkWeight {
		ZERO_WEIGHT("0-link-weight"), ONE_OVER_TWO_WEIGHT(
				"1-over-2-link-weight"), ONE_OVER_N_WEIGHT(
				"1-over-n-link-weight");

		private final String path;

		LinkWeight(String path) {
			this.path = path;
		}
	}
}
