package ir.wiki;

import ir.wiki.Configuration.LinkWeight;
import ir.wiki.Configuration.ModelType;
import ir.wiki.core.ModelManager;
import ir.wiki.core.model.QLModel;
import ir.wiki.eval.Evaluation;
import ir.wiki.query.IRQuery;
import ir.wiki.sim.SimMeasure;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;

/**
 * 
 * @author Ryan
 *
 */
public class Wiki {
	public static void main(String[] args) throws WikiApiException,
			IOException, ClassNotFoundException {

		List<ModelType> modelTypes = new ArrayList<>();
		// modelTypes.add(ModelType.UNSMOOTH);
		// modelTypes.add(ModelType.LINDSTONE);
		modelTypes.add(ModelType.WITTENBELL);

		List<LinkWeight> linkWeights = new ArrayList<>();
		linkWeights.add(LinkWeight.ONE_OVER_N_WEIGHT);
		linkWeights.add(LinkWeight.ONE_OVER_TWO_WEIGHT);
		linkWeights.add(LinkWeight.ZERO_WEIGHT);
		for (ModelType modelType : modelTypes) {
			for (LinkWeight linkWeight : linkWeights) {
				System.out.println("\n\n--------------");
				Configuration config = new Configuration.Builder()
						.modelType(modelType).linkWeight(linkWeight).build();

				ModelManager wikiModel = ModelManager.buildWikiModel(config);
				ModelManager docModel = ModelManager.buildDocumentModel(config);

				IRQuery queries = IRQuery.buildQueryFromFile();
				System.out.println("Configuration: " + modelType + ", "
						+ linkWeight);

				int queryId = 300;
				int correctCount = 0;
				for (String query : queries.getRawQueries()) {
					System.out.println("Query: " + query);
					List<Map.Entry<String, Double>> topDocuments = Wiki
							.getTopDocuments(wikiModel.getModels(),
									docModel.getModels(), query, 10);
					// queryId++;
					// for (Map.Entry<String, Double> topDoc : topDocuments) {
					// System.out
					// .print(topDoc.getKey()
					// + ":"
					// + Evaluation.isSimilar(queryId,
					// topDoc.getKey()) + ", ");
					// }
					// System.out.println();
					System.out.println(topDocuments);
					for (Map.Entry<String, Double> entry : topDocuments) {
						if (entry.getKey().contains(query)) {
							correctCount++;
						}
					}
				}
				System.out.println(correctCount);
			}
		}
	}

	/**
	 * Helper method to return the top scored id's from a map.
	 * 
	 * This method essentially sort the map based on the value
	 * 
	 * @param idScoreMap
	 * @param limit
	 * @return
	 */
	private static List<Map.Entry<String, Double>> getTopScores(
			Map<String, Double> idScoreMap, int limit) {
		List<Map.Entry<String, Double>> scoreList = new ArrayList<>(
				idScoreMap.entrySet());

		return scoreList
				.stream()
				.sorted(Comparator.comparing(
						Map.Entry<String, Double>::getValue).reversed())
				.limit(limit).collect(Collectors.toList());
	}

	/**
	 * 
	 * @param idScoreMap
	 * @param limit
	 * @return
	 */
	private static List<Map.Entry<String, Double>> getLowScores(
			Map<String, Double> idScoreMap, int limit) {
		List<Map.Entry<String, Double>> scoreList = new ArrayList<>(
				idScoreMap.entrySet());

		return scoreList
				.stream()
				.sorted(Comparator
						.comparing(Map.Entry<String, Double>::getValue))
				.limit(limit).collect(Collectors.toList());
	}

	/**
	 * return a map of unranked top models <model key, score> ...
	 *
	 * @param models
	 * @param query
	 * @param count
	 * @return
	 */
	private static String getBestModel(Map<String, QLModel> models, String query) {

		Map<String, Double> allScores = new HashMap<>();

		for (String key : models.keySet()) {
			QLModel modelPage = models.get(key);
			double score = SimMeasure.QueryModelSimilarity(query, modelPage);
			allScores.put(key, score);
		}

		return getTopScores(allScores, 1).get(0).getKey();
	}

	public static List<Map.Entry<String, Double>> getTopDocuments(
			Map<String, QLModel> wikiModels, Map<String, QLModel> docModels,
			String query, int limit) throws NumberFormatException,
			WikiApiException {
		// the wiki page id that is most similar to the query
		String bestWikiId = getBestModel(wikiModels, query);
		System.out.println("best wiki page: "
				+ WikiAccessor.getConnection().getTitle(
						Integer.parseInt(bestWikiId)));
		QLModel bestWikiPage = wikiModels.get(bestWikiId);

		Map<String, Double> allDocDivergence = new HashMap<>();
		for (String docName : docModels.keySet()) {
			QLModel docPage = docModels.get(docName);
			double divergence = SimMeasure.klDivergence(bestWikiPage, docPage);

			allDocDivergence.put(docName, divergence);
		}

		return getLowScores(allDocDivergence, limit);
	}
}
