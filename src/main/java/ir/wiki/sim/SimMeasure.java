package ir.wiki.sim;

import ir.wiki.Configuration.ModelType;
import ir.wiki.core.model.QLModel;
import ir.wiki.prep.WikiTextPreprocessor;

import java.util.ArrayList;
import java.util.List;

import org.la4j.vector.SparseVector;
import org.la4j.vector.functor.VectorProcedure;

public class SimMeasure {

	private SimMeasure() {
	}

	public static double QueryModelSimilarity(String query, QLModel modelQLPage) {
		String[] tokenizedText = WikiTextPreprocessor.preprocess(query);

		QLModel queryBinaryPage = QLModel.buildPage(ModelType.BINARY,
				tokenizedText);

		SparseVector queryVector = queryBinaryPage.getVector();

		// get the index of nonzero elements from the binary model
		final List<Integer> nonZeroQueryIndexes = new ArrayList<>();
		queryVector.eachNonZero(new VectorProcedure() {

			@Override
			public void apply(int i, double value) {
				nonZeroQueryIndexes.add(i);
			}
		});

		SparseVector modelVector = modelQLPage.getVector();
		double simScore = 0;

		// equivalent to the inner product of two vectors, but we take care of
		// the smoothing factor here
		for (int nonzeroIndex : nonZeroQueryIndexes) {
			simScore += modelVector.getOrElse(nonzeroIndex,
					modelQLPage.getZeroSmooth(nonzeroIndex));
		}

		return simScore;
	}

	/**
	 * 
	 * @param trueDist
	 *            true distribution
	 * @param estDist
	 *            estimated distribution
	 * @return kl divergence score
	 */
	public static double klDivergence(QLModel trueDist, QLModel estDist) {
		SparseVector trueVect = trueDist.getVector();
		SparseVector estVect = estDist.getVector();

		double score = 0;
		// for each feature
		for (int i = 0; i < trueVect.length(); i++) {
			double trueOfx = trueVect.getOrElse(i, trueDist.getZeroSmooth(i));
			double estOfx = estVect.getOrElse(i, estDist.getZeroSmooth(i));
			if (estOfx == 0 || trueOfx == 0) {
				score += 0;
			} else {
				score += trueOfx * Math.log(trueOfx / estOfx);
			}
		}

		return score;
	}
}
