package ir.wiki.sim;

import ir.wiki.core.model.QLModel;

public interface SimMeasureProcedure {
	void apply(QLModel baseModel);
}