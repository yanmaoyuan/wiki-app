package ir.wiki.prep;

import java.util.HashMap;
import java.util.Map;

import com.aliasi.tokenizer.EnglishStopTokenizerFactory;
import com.aliasi.tokenizer.LowerCaseTokenizerFactory;
import com.aliasi.tokenizer.PorterStemmerTokenizerFactory;
import com.aliasi.tokenizer.RegExTokenizerFactory;
import com.aliasi.tokenizer.Tokenizer;
import com.aliasi.tokenizer.TokenizerFactory;

public class WikiTextPreprocessor {
	private static final TokenizerFactory TOKEN_FACTORY;
	private static final String ALPHA_NUM = "[a-zA-Z|'|-]+|[0-9]+";

	static {
		TOKEN_FACTORY = new PorterStemmerTokenizerFactory(
				new EnglishStopTokenizerFactory(new LowerCaseTokenizerFactory(
						new RegExTokenizerFactory(ALPHA_NUM))));
	}

	public static String[] preprocess(String text) {
		char[] textArray = text.toCharArray();

		Tokenizer tokenizer = TOKEN_FACTORY.tokenizer(textArray, 0,
				textArray.length);

		return tokenizer.tokenize();
	}

	/**
	 * tokenize and preprocess
	 * 
	 * @param text
	 * @return
	 */
	public static Map<String, Integer> preprocessWordCount(String text) {

		return preprocessWordCount(preprocess(text));
	}

	/**
	 * hashmap of a word and its count in a document
	 * 
	 * @param tokenizedText
	 * @return
	 */
	public static Map<String, Integer> preprocessWordCount(
			String[] tokenizedText) {

		Map<String, Integer> termCount = new HashMap<>();
		// for each word, count the number of occurances
		for (String word : tokenizedText) {
			termCount.put(word, termCount.getOrDefault(word, 0) + 1);
		}

		return termCount;
	}

}
