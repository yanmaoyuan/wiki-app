package ir.wiki.query;

import ir.wiki.Configuration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IRQuery {
	private final List<String> rawQueries;

	private IRQuery(List<String> tokenizedQueries) {
		this.rawQueries = tokenizedQueries;
	}

	public static IRQuery buildQueryFromFile() {
		File queryFile = Configuration.QUERY_FILE;
		List<String> queries = new ArrayList<>();
		try (Scanner scanner = new Scanner(new FileReader(queryFile))) {

			String rawQuery;
			while (scanner.hasNextLine()) {
				// process each line as a query
				rawQuery = scanner.nextLine();
				queries.add(rawQuery);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new IRQuery(queries);
	}

	public List<String> getRawQueries() {
		return rawQueries;
	}
}
