package ir.wiki.eval;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Evaluation {
	// query ID -> obtained filename
	private static Map<Integer, Map<String, SimStatus>> queryDocTruthMap;
	static {
		queryDocTruthMap = new HashMap<>();
		try (Scanner scanner = new Scanner(new FileReader(
				"data/query/qres1.txt"))) {

			while (scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\\s");
				int queryId = Integer.parseInt(tokens[0]);
				String docName = tokens[2];
				boolean isSimilar = Integer.parseInt(tokens[3]) == 1;

				if (!queryDocTruthMap.containsKey(queryId)) {
					queryDocTruthMap.put(queryId, new HashMap<>());
				}
				queryDocTruthMap.get(queryId).put(docName,
						isSimilar ? SimStatus.SIMILAR : SimStatus.DISSIMILAR);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public static SimStatus isSimilar(int queryId, String docName) {
		if (queryDocTruthMap.get(queryId).containsKey(docName)) {
			return queryDocTruthMap.get(queryId).get(docName);
		}
		return SimStatus.UNKNWON;
	}

	public enum SimStatus {
		SIMILAR, DISSIMILAR, UNKNWON;
	}
}
