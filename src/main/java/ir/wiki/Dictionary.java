package ir.wiki;

import ir.wiki.prep.WikiTextPreprocessor;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import de.tudarmstadt.ukp.wikipedia.api.Page;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;

public class Dictionary implements Serializable {

	private static final long serialVersionUID = 1L;
	private static Dictionary instance;
	private static final File DICTIONARY_FILE = Configuration.DICTIONARY_FILE;
	// tree map, use this one whenever the vocabulary needs to be in sequence
	private Map<String, IndexCount> vocabIndexCount;
	// map index to vocabulary (for convenience purposes)
	private Map<Integer, String> indexVocab;
	private int totalWordCount;

	/**
	 * Singleton, constructor should not be called outside of the builder method
	 * 
	 * @param dictionary
	 * @param indexVocab
	 */
	private Dictionary(Map<String, IndexCount> dictionary,
			Map<Integer, String> indexVocab, int totalWordCount) {
		this.vocabIndexCount = dictionary;
		this.indexVocab = indexVocab;
		this.totalWordCount = totalWordCount;
	}

	public static Dictionary getInstance() {
		if (instance == null) {
			/*
			 * if cache exists, build using the cache, otherwise build it
			 */
			if (DICTIONARY_FILE.exists()) {

				instance = fromCache();
			} else {

				instance = fromScratch();
				/* cache the dictionary */
				instance.cacheResult();
			}
		}
		return instance;
	}

	/**
	 * Cache the result
	 */
	private void cacheResult() {
		try (FileOutputStream fos = new FileOutputStream(DICTIONARY_FILE);
				ObjectOutputStream oos = new ObjectOutputStream(fos)) {
			oos.writeObject(instance);
		} catch (IOException e) {
			System.err.println(e.getMessage());
			System.exit(1);
		}
	}

	private static Dictionary fromScratch() {
		/*
		 * in the first run, store everything in a map of <vocab : count>,
		 * because we do not know the index of each vocabulary until later
		 */
		Map<String, Integer> vocabCount = new TreeMap<>();
		// after vocabCOunt is filled, fill the dictionary
		Map<String, IndexCount> dictionary = new TreeMap<>();
		Iterable<Page> pages = WikiAccessor.getConnection().getArticles();
		String[] tokenizedTerms = null;
		int count = 0;
		// count the total number of words that are used to build the dictionary
		int totalWordCount = 0;
		for (Page page : pages) {
			try {
				tokenizedTerms = WikiTextPreprocessor.preprocess(page
						.getPlainText());

				totalWordCount += tokenizedTerms.length;

				System.out.println("dictionary: " + count++);
				// first add each vocabulary to the TreeMap to eliminate
				// duplications and assure ordering
				vocabCount.putAll(WikiTextPreprocessor
						.preprocessWordCount(tokenizedTerms));

			} catch (WikiApiException e) {
				// TODO Auto-generated catch block
				System.err.println(e.getMessage());
				continue;
			}
		}
		// after the vocab count is filled. . the reason that we use
		// TreeMap<word : index&count> is because
		// we will need to use index fpr calculating binarymodel, and
		// use
		// count for smoothing
		// map of indices to vocabulary
		Map<Integer, String> indexVocab = new HashMap<>(vocabCount.size());
		int index = 0;
		for (String vocab : vocabCount.keySet()) {
			dictionary.put(vocab, new IndexCount(index, vocabCount.get(vocab)));
			indexVocab.put(index, vocab);
			index++;
		}
		return new Dictionary(dictionary, indexVocab, totalWordCount);
	}

	/**
	 * read from the cached result
	 * 
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private static Dictionary fromCache() {
		Dictionary dictionary = null;
		try (FileInputStream fis = new FileInputStream(DICTIONARY_FILE);
				ObjectInputStream ois = new ObjectInputStream(fis)) {
			dictionary = (Dictionary) ois.readObject();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return dictionary;
	}

	private static class IndexCount implements Serializable {
		private static final long serialVersionUID = 1L;
		private int index;
		private int count;

		public IndexCount(int index, int count) {
			this.index = index;
			this.count = count;
		}
	}

	/**
	 * check whether a word is in a dictionary
	 * 
	 * @param word
	 * @return
	 */
	public boolean contains(String word) {
		return vocabIndexCount.containsKey(word);
	}

	/**
	 * size of the dictionary
	 * 
	 * @return
	 */
	public int size() {
		return vocabIndexCount.size();
	}

	/**
	 * The index of the word that appeared in the dictionary set.
	 * 
	 * The index matches the features built based using the dictionary
	 * 
	 * @param word
	 * @return
	 */
	public int indexOf(String word) {
		if (!vocabIndexCount.containsKey(word)) {
			throw new IllegalArgumentException(word
					+ " is not in the dictionary");
		}

		return vocabIndexCount.get(word).index;
	}

	/**
	 * the total amount of a word that appear in all of the documents that the
	 * dictionary was built with. (used in smoothing)
	 * 
	 * @param word
	 * @return
	 */
	public int countOf(String word) {
		if (!vocabIndexCount.containsKey(word)) {
			return 0;
		}

		return vocabIndexCount.get(word).count;
	}

	public String vocabOf(int index) {
		if (!indexVocab.containsKey(index)) {
			throw new IllegalArgumentException("index is out of range: "
					+ index);
		}

		return indexVocab.get(index);
	}

	public Set<String> vocabularies() {
		return vocabIndexCount.keySet();
	}

	/**
	 * total number of words that were used to build the dictionary NOTE:
	 * different from dictionary length
	 * 
	 * @return
	 */
	public int getTotalWordCount() {
		return totalWordCount;
	}

}
