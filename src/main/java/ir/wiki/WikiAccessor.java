package ir.wiki;

import de.tudarmstadt.ukp.wikipedia.api.DatabaseConfiguration;
import de.tudarmstadt.ukp.wikipedia.api.Wikipedia;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiInitializationException;

/**
 * 
 * Connection to the wikipedia database. The database is built using the simple
 * wikipedia dump files
 * 
 * @author Ryan
 *
 */
public class WikiAccessor extends Wikipedia {
	private static final String HOST = "localhost";
	private static final String DATABASE = "WikiEngine";
	private static final String USER = "root";
	private static final String PASS = "root";
	private static WikiAccessor instance;

	private WikiAccessor(DatabaseConfiguration dbConfig)
			throws WikiInitializationException {
		super(dbConfig);
	}

	public static WikiAccessor getConnection() {
		if (instance == null) {
			DatabaseConfiguration dbConfig = new DatabaseConfiguration();
			dbConfig.setHost(HOST);
			dbConfig.setDatabase(DATABASE);
			dbConfig.setUser(USER);
			dbConfig.setPassword(PASS);
			dbConfig.setLanguage(Language.english);

			try {
				instance = new WikiAccessor(dbConfig);
			} catch (WikiInitializationException e) {
				e.printStackTrace();
			}
		}

		return instance;
	}

}
