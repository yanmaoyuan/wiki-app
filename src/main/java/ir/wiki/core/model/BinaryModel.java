package ir.wiki.core.model;

import ir.wiki.prep.WikiTextPreprocessor;

import java.util.Map;

import org.la4j.vector.SparseVector;
import org.la4j.vector.sparse.CompressedVector;

public class BinaryModel extends QLModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4006630858595194880L;

	public BinaryModel(String[] tokenizedText) {
		super(tokenizedText);
	}

	@Override
	public double getZeroSmooth(int index) {
		throw new UnsupportedOperationException("binary model does not smooth");
	}

	@Override
	protected SparseVector buildVector(String[] tokenizedText) {
		// create a model vector for each file
		SparseVector vector = CompressedVector.zero(dictionary.size());
		Map<String, Integer> termCount = WikiTextPreprocessor
				.preprocessWordCount(tokenizedText);

		for (String word : termCount.keySet()) {
			if (dictionary.contains(word)) {
				vector.set(dictionary.indexOf(word), 1);
			}
		}

		return vector;
	}
}
