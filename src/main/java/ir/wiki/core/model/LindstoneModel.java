package ir.wiki.core.model;

import ir.wiki.prep.WikiTextPreprocessor;

import java.util.Map;

import org.la4j.vector.SparseVector;
import org.la4j.vector.sparse.CompressedVector;

public class LindstoneModel extends QLModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4209494426687922718L;
	private static final double EPSILON = 1.0;
	private double zeroSmooth;

	public LindstoneModel(String[] tokenizedText) {
		super(tokenizedText);
		this.zeroSmooth = EPSILON / (totalWords + uniqWords * EPSILON);
	}

	@Override
	public double getZeroSmooth(int index) {
		return zeroSmooth;
	}

	@Override
	protected SparseVector buildVector(String[] tokenizedText) {
		// for each word, count the number of occurances
		Map<String, Integer> termCount = WikiTextPreprocessor
				.preprocessWordCount(tokenizedText);

		// create a model vector for each file
		SparseVector vector = new CompressedVector(dictionary.size());
		// length of the document
		int docLength = tokenizedText.length;
		// vocab size
		int vocabSize = dictionary.size();
		// botton part of the formula |d_j| + N*epsilon
		double divisor = (docLength + vocabSize * EPSILON);
		for (String word : termCount.keySet()) {
			if (dictionary.contains(word)) {

				double smoothed = (EPSILON + termCount.get(word)) / divisor;

				// use index because we do not need to set each element in the
				// sparse vector.
				int index = dictionary.indexOf(word);
				// calculate the query likelyhood for the word
				vector.set(index, smoothed);
			}
		}

		return vector;
	}

}
