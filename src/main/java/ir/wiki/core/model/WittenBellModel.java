package ir.wiki.core.model;

import ir.wiki.prep.WikiTextPreprocessor;

import java.util.Arrays;
import java.util.Map;

import org.la4j.vector.SparseVector;
import org.la4j.vector.sparse.CompressedVector;

public class WittenBellModel extends QLModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1580871008721520152L;

	public WittenBellModel(String[] tokenizedText) {
		super(tokenizedText);
	}

	@Override
	public double getZeroSmooth(int index) {
		int grandFreq = dictionary.countOf(dictionary.vocabOf(index));
		if (grandFreq == 0) {
			System.err.println(index + dictionary.vocabOf(index));
			System.exit(1);
		}
		return ((double) uniqWords / (totalWords + uniqWords))
				* ((double) grandFreq / dictionary.getTotalWordCount());
	}

	@Override
	protected SparseVector buildVector(String[] tokenizedText) {
		// for each word, count the number of occurances
		Map<String, Integer> termCount = WikiTextPreprocessor
				.preprocessWordCount(tokenizedText);

		// length of the document
		int docLength = tokenizedText.length;
		// create a model vector for each file
		SparseVector vector = new CompressedVector(dictionary.size());
		// number of unique terms in the document
		int uniqTerms = termCount.size();

		// calculate |d_j|/(|d_j| + u_j)
		double part1 = ((double) docLength / (docLength + uniqTerms));
		// u_j / (|d_j| + u_j)
		double part3 = (double) uniqTerms / (docLength + uniqTerms);

		for (String word : termCount.keySet()) {
			if (dictionary.contains(word)) {
				// f_ij / |d_j|
				double part2 = (double) termCount.get(word) / docLength;
				// u_j/(|d_j| + u_j)
				double part4 = (double) dictionary.countOf(word)
						/ dictionary.getTotalWordCount();

				// use index because we do not need to set each element in the
				// sparse vector.
				int index = dictionary.indexOf(word);
				// calculate the query likelihood for the word
				vector.set(index, part1 * part2 + part3 * part4);
			}
		}

		return vector;
	}
}
