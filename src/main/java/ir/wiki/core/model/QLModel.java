package ir.wiki.core.model;

import ir.wiki.Configuration.ModelType;
import ir.wiki.Dictionary;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.la4j.vector.SparseVector;
import org.la4j.vector.functor.VectorProcedure;
import org.la4j.vector.sparse.CompressedVector;

public abstract class QLModel implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6337637901795731452L;
	protected static final Dictionary dictionary = Dictionary.getInstance();
	protected transient SparseVector vector;
	// length of the document
	protected final int totalWords;
	// unique words in vocab
	protected final int uniqWords;

	protected QLModel(String[] tokenizedText) {
		// vector should be the first thing getting building
		this.vector = buildVector(tokenizedText);
		this.totalWords = tokenizedText.length;
		this.uniqWords = vector.cardinality();
	}

	/**
	 * Build the page based on the algorithm given
	 * 
	 * @param algorithm
	 * @param tokenizedText
	 * @return
	 */
	public static QLModel buildPage(ModelType modelType, String[] tokenizedText) {

		switch (modelType) {
		case BINARY:

			return new BinaryModel(tokenizedText);
		case UNSMOOTH:

			return new UnsmoothModel(tokenizedText);
		case WITTENBELL:

			return new WittenBellModel(tokenizedText);
		case LINDSTONE:

			return new LindstoneModel(tokenizedText);

		default:
			throw new IllegalArgumentException("Unknown model type: "
					+ modelType);
		}
	}

	/**
	 * Because SparseVector does not implement serializable. We serialize it by
	 * overriding writeobejct method
	 * 
	 * @param os
	 * @throws IOException
	 */
	private void writeObject(ObjectOutputStream os) throws IOException {

		os.defaultWriteObject();

		// since sparsevector is not serializable, serialize it into a map
		Map<Integer, Double> nonzeroVector = new HashMap<>(vector.cardinality());
		vector.eachNonZero(new VectorProcedure() {

			@Override
			public void apply(int i, double value) {
				nonzeroVector.put(i, value);
			}

		});
		os.writeObject(nonzeroVector);
	}

	/**
	 * Because SparseVector does not implement serializable. we deserialize it
	 * by readObject
	 * 
	 * @param is
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	private void readObject(ObjectInputStream is) throws IOException,
			ClassNotFoundException {
		is.defaultReadObject();
		@SuppressWarnings("unchecked")
		Map<Integer, Double> nonzeros = (HashMap<Integer, Double>) is
				.readObject();
		// resolve the vector deserialization
		vector = new CompressedVector(dictionary.size());
		for (int index : nonzeros.keySet()) {
			vector.set(index, nonzeros.get(index));
		}
	}

	/**
	 * The algorithm for building the specific vector based on the specific
	 * requirement, such as the type of smoothing required
	 * 
	 * @param tokenizedText
	 * @return
	 */
	protected abstract SparseVector buildVector(String[] tokenizedText);

	public abstract double getZeroSmooth(int index);

	public SparseVector getVector() {
		return vector;
	}
}
