package ir.wiki.core.model;

import ir.wiki.prep.WikiTextPreprocessor;

import java.util.Map;

import org.la4j.vector.SparseVector;
import org.la4j.vector.sparse.CompressedVector;

public class UnsmoothModel extends QLModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6812680777651305609L;

	public UnsmoothModel(String[] tokenizedText) {
		super(tokenizedText);
	}

	@Override
	public double getZeroSmooth(int index) {
		return 0;
	}

	@Override
	protected SparseVector buildVector(String[] tokenizedText) {
		// for each word, count the number of occurances
		Map<String, Integer> termCount = WikiTextPreprocessor
				.preprocessWordCount(tokenizedText);

		// use index because we do not need to set each element in the
		// sparse vector.
		int index = 0;
		int docLength = tokenizedText.length;
		// create a model vector for each file
		SparseVector vector = CompressedVector.zero(dictionary.size());
		for (String word : termCount.keySet()) {
			// add to the vector only if the word is in the dictionary
			if (dictionary.contains(word)) {
				// index to insert word into the vector
				// the index correspond to the dictionary index
				index = dictionary.indexOf(word);
				// calculate the query likelyhood for the word
				vector.set(index, (double) termCount.get(word) / docLength);
			}

		}

		return vector;
	}
}
