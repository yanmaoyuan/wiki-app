package ir.wiki.core;

import ir.wiki.Configuration.LinkWeight;
import ir.wiki.Configuration.ModelType;
import ir.wiki.WikiAccessor;
import ir.wiki.core.model.QLModel;
import ir.wiki.prep.WikiTextPreprocessor;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.la4j.vector.SparseVector;

import de.tudarmstadt.ukp.wikipedia.api.Page;
import de.tudarmstadt.ukp.wikipedia.api.exception.WikiApiException;

public final class WikiModelManager extends ModelManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5382655049493768326L;

	public WikiModelManager(Map<String, QLModel> model) {
		super(model);
	}

	/**
	 * Build the model file from scratch and write encoded file to a file
	 * 
	 * @param pages
	 * @throws WikiApiException
	 */
	public static ModelManager buildModelManagerFromScratch(
			ModelType modelType, LinkWeight linkWeight) {
		// Store the ultimate documents
		Map<String, QLModel> documents = new HashMap<>();
		WikiAccessor accessor = WikiAccessor.getConnection();
		Iterable<Page> pages = accessor.getArticles();

		int count = 0;
		for (Page page : pages) {
			// get all of the words from a page
			String[] tokenizedText;

			try {
				System.out.println("building model: " + ++count);
				tokenizedText = WikiTextPreprocessor.preprocess(page
						.getPlainText());

				/*
				 * Realized that some of the pages are empty after processing
				 * them, therefore, ignore those pages
				 */
				if (tokenizedText.length != 0) {
					// build the query
					QLModel qlPage = QLModel
							.buildPage(modelType, tokenizedText);

					// build the modelvector
					documents.put(String.valueOf(page.getPageId()), qlPage);
				}
				// ignor the page is it fails to process
			} catch (WikiApiException e) {
				System.err.println("error at processing raw text: "
						+ e.getMessage());
			}
		}

		/*
		 * adding the direct linked pages (incoming and outgoing) as part of the
		 * model. The weight of the linked pages are half of their original
		 * weights
		 */
		applyLinkWeight(documents, linkWeight);

		ModelManager model = new WikiModelManager(documents);

		return model;
	}

	private static void applyLinkWeight(Map<String, QLModel> documents,
			LinkWeight linkWeight) {
		if (linkWeight == LinkWeight.ZERO_WEIGHT) {
			return;
		}
		/*
		 * adding the direct linked pages (incoming and outgoing) as part of the
		 * model. The weight of the linked pages are half of their original
		 * weights
		 */
		for (String pageId : documents.keySet()) {
			// apply the incoming and outgoing vectors to this vector
			SparseVector modelVector = documents.get(pageId).getVector();
			Set<Integer> links = new HashSet<>();
			try {
				// get all of the incoming and outgoing links
				Page page = WikiAccessor.getConnection().getPage(
						Integer.parseInt(pageId));
				links.addAll(page.getInlinkIDs());
				links.addAll(page.getOutlinkIDs());

				double weightFactor;
				/*
				 * Based on the linkweight scheme to apply the according weights
				 * to the links
				 */
				switch (linkWeight) {
				case ONE_OVER_N_WEIGHT:
					weightFactor = links.size();
					break;
				case ONE_OVER_TWO_WEIGHT:
					weightFactor = 2;
					break;
				default:
					throw new IllegalArgumentException(
							"unexpected weighting scheme");
				}

				for (int linkId : links) {
					if (documents.containsKey(String.valueOf(linkId))) {
						SparseVector linkedVector = (SparseVector) documents
								.get(String.valueOf(linkId)).getVector().copy();
						modelVector = (SparseVector) modelVector
								.add(linkedVector.multiply(1 / weightFactor));
					}
				}

			} catch (NumberFormatException | WikiApiException e) {
				System.err.println("unexpected error with extracting links"
						+ e.getMessage());
				System.exit(1);
			}
		}
	}

}
