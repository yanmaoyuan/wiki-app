package ir.wiki.core;

import ir.wiki.Configuration;
import ir.wiki.Configuration.ModelType;
import ir.wiki.core.model.QLModel;
import ir.wiki.prep.WikiTextPreprocessor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * The document model
 * 
 * @author Ryan
 *
 */
public class DocModelManager extends ModelManager {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3994830550109053215L;

	public DocModelManager(Map<String, QLModel> model) {
		super(model);
	}

	/**
	 * 
	 * @return
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static DocModelManager buildModelManagerFromScratch(
			ModelType modelType) {
		File fileDir = Configuration.DOCUMENT_DIR.toFile();
		Map<String, QLModel> documentModel = new HashMap<>(
				fileDir.listFiles().length);

		String rawText;
		String[] tokenizedText;
		for (File file : fileDir.listFiles()) {
			try {
				// read the file into a string
				rawText = new String(Files.readAllBytes(file.toPath()));
				// tokenize the documents
				tokenizedText = WikiTextPreprocessor.preprocess(rawText);

				// in case of empty documents after processing
				if (tokenizedText.length != 0) {
					// calculate the querylikelyhood vector for the text
					// document
					QLModel qlPage = QLModel
							.buildPage(modelType, tokenizedText);
					// add the generated QL model to the document model
					documentModel.put(file.getName(), qlPage);
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// generate the model class
		DocModelManager model = new DocModelManager(documentModel);
		return model;
	}

}
