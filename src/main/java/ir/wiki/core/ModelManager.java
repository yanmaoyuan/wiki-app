package ir.wiki.core;

import ir.wiki.Configuration;
import ir.wiki.Dictionary;
import ir.wiki.core.model.QLModel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Map;

/**
 * Base model for both wikipedia model and the document model
 * 
 * @author Ryan
 *
 */
public abstract class ModelManager implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 675454139207459760L;
	/* <document id: <p(word1), p(word2), p(word3)...>> */
	protected final Map<String, QLModel> models;

	protected ModelManager(Map<String, QLModel> models) {
		this.models = models;
	}

	/**
	 * This method will build the wikimodel.
	 * 
	 * It tries to read the cached binary file to build the model If the file
	 * does not exist, it tries the to build the Model in scratch and cache the
	 * result for the future optimization purposes
	 * 
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	public static ModelManager buildWikiModel(Configuration config)
			throws IOException, ClassNotFoundException {
		File wikiModelFile = config.getWikiModelFile();
		// if the model file has not been cached yet
		if (!wikiModelFile.exists()) {
			// build the model file from scratch
			ModelManager wikiModel = WikiModelManager
					.buildModelManagerFromScratch(config.modelType(),
							config.linkWeightType());

			// cache the result
			wikiModel.cacheModel(wikiModelFile);
			return wikiModel;
		}

		return fromCacheFile(wikiModelFile);
	}

	/**
	 * This method will build the DocumentModel.
	 * 
	 * It tries to read the cached binary file to build the model If the file
	 * does not exist, it tries the to build the Model in scratch and cache the
	 * result for the future optimization purposes
	 * 
	 * @return
	 * @throws IOException
	 * @throws ClassNotFoundException
	 * @throws FileNotFoundException
	 */
	public static ModelManager buildDocumentModel(Configuration config)
			throws FileNotFoundException, ClassNotFoundException, IOException {
		File documentModelFile = config.getDocumentModelFile();
		// if the model file has not been cached yet
		if (!documentModelFile.exists()) {
			// build the model file from scratch
			ModelManager document = DocModelManager
					.buildModelManagerFromScratch(config.modelType());

			// cache the model file
			document.cacheModel(documentModelFile);
			return document;
		}

		return fromCacheFile(documentModelFile);
	}

	/**
	 * Cache the model
	 * 
	 * The model cache currently supports DocumentModel and WikiModel as they
	 * are large
	 * 
	 * @param output
	 * @throws IOException
	 */
	protected void cacheModel(File output) throws IOException {
		try (OutputStream fileOut = new FileOutputStream(output);
				ObjectOutputStream out = new ObjectOutputStream(fileOut)) {
			out.writeObject(this);
			out.flush();
		}
	}

	/**
	 * After the object has been cached before, it simply loads the cache for
	 * efficiency purposes
	 * 
	 * @param input
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ClassNotFoundException
	 */
	protected static ModelManager fromCacheFile(File input)
			throws FileNotFoundException, IOException, ClassNotFoundException {
		try (InputStream in = new FileInputStream(input);
				ObjectInputStream objInput = new ObjectInputStream(in)) {
			return (ModelManager) objInput.readObject();
		}
	}

	public Map<String, QLModel> getModels() {
		return models;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Document Size: " + models.size() + "\n");
		builder.append("feature size: " + Dictionary.getInstance().size());

		return builder.toString();
	}

}
